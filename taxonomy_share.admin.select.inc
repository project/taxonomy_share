<?php
// $Id: taxonomy_share.admin.select.inc $
/**
 * @File
 * Choose vocabularies to import from and to.
 */

module_load_include('admin.inc', 'taxonomy_share', 'taxonomy_share');

/**
 * Form for selecting a remote vocabulary to import from and a local vocabulary to import to.
 */
function category_import_type($form_state) {
  //drupal_get_messages();
  $form = array();

  $form['site_taxonomy'] = array(
    '#type' => 'fieldset',
    '#description' => 'Choose which vocabulary to import from the remote site, and which existing vocabulary to populate on the current site.',
  );

  // Setting $db_url.
  db_set_active('taxonomy_import');

  $vocabulary_list_remote = taxonomy_get_vocabularies();
  $options = array('' => t('Choose One'));
  foreach ($vocabulary_list_remote as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }

  $form['site_taxonomy']['vocabulary_remote'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary to get from remote site'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('taxonomy_share_remote', ''),
  );
  
  // set back to site's default database
  db_set_active('default');

  $vocabulary_list = taxonomy_get_vocabularies();
  $options = array('' => t('Choose One'));
  foreach ($vocabulary_list as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }

  $form['site_taxonomy']['vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary to populate on this site. <br />If you want to use a new vocabulary, please go to the Taxonomy ' . l( t('Add Vocabulary admin Page'), 'admin/content/taxonomy/add/vocabulary')),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('taxonomy_share_local', ''),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Categories'),
  );

  return $form;
}

function category_import_type_submit($form, &$form_state) {
  drupal_get_messages();

  variable_set('taxonomy_share_remote', $form_state['values']['vocabulary_remote']);
  variable_set('taxonomy_share_local', $form_state['values']['vocabulary']);
  $remote_vid = variable_get('taxonomy_share_remote', '');
  $local_vid = variable_get('taxonomy_share_local', '');

  // Set the remote site's database as active.
  db_set_active('taxonomy_import');
  $tree = taxonomy_get_tree($remote_vid);

  db_set_active('default');
  $hierarchy = 0;
  foreach ($tree as $term) {
    $remote_tid = $term->tid;
    $remote_name = $term->name;
    $remote_description = $term->description;
    $remote_parentid = $term->parents[0];

    $count_query = "SELECT count(*) FROM {term_data} t
                    INNER JOIN {taxonomy_share_terms} tts ON t.tid = tts.tid
                    WHERE tts.remote_vid = %d and tts.remote_tid = %d";
    $count_result = db_result(db_query($count_query, $remote_vid, $remote_tid));
    
    $query = "SELECT t.tid, t.vid, t.name, t.description, t.weight, parent FROM {term_data} t
              INNER JOIN {term_hierarchy} h ON t.tid = h.tid
              INNER JOIN {taxonomy_share_terms} tts on t.tid = tts.tid
              WHERE tts.remote_vid = %d and tts.remote_tid = %d";
    $result = db_query(db_rewrite_sql($query, 't', 'tid'), $remote_vid, $remote_tid);
    
    $query = "SELECT t.tid, t.vid, t.name, t.description, t.weight, remote_parentid FROM {term_data} t
              INNER JOIN {taxonomy_share_terms} tts on t.tid = tts.tid
              WHERE tts.remote_vid = %d and tts.remote_tid = %d";
    $result = db_query(db_rewrite_sql($query, 't', 'tid'), $remote_vid, $remote_tid);
    
    if ($count_result == 0) {
      db_query("INSERT INTO {term_data} (vid, name, description)
                VALUES (%d, '%s', '%s')", $local_vid, $remote_name, $remote_description);
      db_query("INSERT INTO {taxonomy_share_terms} (tid, remote_vid, remote_tid, remote_parentid)
                VALUES ((SELECT max(tid) from {term_data}), %d, %d, %d)", $remote_vid, $remote_tid, $remote_parentid);
      if ($remote_parentid == 0) {
      // Insert record into hierarchy table
        db_query("INSERT INTO {term_hierarchy} (tid, parent) VALUES
                  ((SELECT t.tid from {term_data} t
                    INNER JOIN {taxonomy_share_terms} s ON t.tid = s.tid
                    WHERE s.remote_tid = %d AND s.remote_vid = %d ), 0)", $remote_tid, $remote_vid);
      }
      else {
        db_query("INSERT INTO {term_hierarchy} (tid, parent) VALUES
                  ((SELECT t.tid from {term_data} t
                    INNER JOIN {taxonomy_share_terms} s ON t.tid = s.tid
                    WHERE s.remote_tid = %d AND s.remote_vid = %d),
                    (SELECT t.tid FROM {term_data} t
                     INNER JOIN {taxonomy_share_terms} s ON t.tid = s.tid
                     WHERE s.remote_tid = %d))", $remote_tid, $remote_vid, $remote_parentid);
      }
    }
    else {
      $termdata = array();
      while ($termdata = db_fetch_object($result)) {
        if ($remote_parentid == 0) {
          db_query("UPDATE {term_data}
                  INNER JOIN {term_hierarchy} ON term_data.tid = term_hierarchy.tid
                  INNER JOIN {taxonomy_share_terms} ON term_data.tid = taxonomy_share_terms.tid
                  SET term_data.name = '%s', term_data.description = '%s', taxonomy_share_terms.remote_parentid = %d,
                  term_data.vid = %d, term_hierarchy.parent = 0
                  WHERE taxonomy_share_terms.remote_vid = %d and taxonomy_share_terms.remote_tid = %d",
                  $remote_name, $remote_description, $remote_parentid, $local_vid, $remote_vid, $remote_tid);
        }
        else {
          $query = "SELECT t1.tid FROM {taxonomy_share_terms} t1 WHERE t1.remote_tid = %d";
          $result = db_query(db_rewrite_sql($query, 't', 'tid'), $remote_parentid);
    
          while ($parentid = db_fetch_object($result)) {
            $parent = $parentid->tid;
          
            db_query("UPDATE {term_data}
                    INNER JOIN {term_hierarchy} ON term_data.tid = term_hierarchy.tid
                    INNER JOIN {taxonomy_share_terms} on term_data.tid = taxonomy_share_terms.tid
                    SET term_data.name = '%s', term_data.description = '%s', taxonomy_share_terms.remote_parentid = %d,
                    term_data.vid = %d, term_hierarchy.parent = %d
                    WHERE taxonomy_share_terms.remote_vid = %d and taxonomy_share_terms.remote_tid = %d",
                    $remote_name, $remote_description, $remote_parentid, $local_vid, $parent, $remote_vid, $remote_tid);
          }
        }
      }
    }
  }
}