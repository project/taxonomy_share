This is a light-weight module that allows you to connect to the database of a remote site and import a vocabulary's taxonomy terms into a current or new vocabulary on the local site.

Assumptions:

You have knowledge of the remote site's database connection information.
You have access to the settings.php file of the local site.
This module allows more updates to the original taxonomy terms to be imported on updates.
Included in install file for this module are three (3) additional fields that are added to the term_data records to record the original tid, vid, and parentid for use with specific theming, or integration in other ways.
Install

Update settings.php $db_url variable to be an array with the values of $db_url['default'] and $db_url['taxomomy_import']:

Example
$db_url['default'] = 'mysqli://username:password@localhost/localdbname';
$db_url['taxonomy_import'] = 'mysqli://username:password@remotehost/remotedbname';
